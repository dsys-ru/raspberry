# Apatit Server for Raspberry Pi

## Usage

Power on Raspberry Pi and wait a minute for backend to start. Then navigate to Raspberry web interface.

## Installation

Installation may take a day or two so be patient.

Install Raspbian console using NOOBS. Then

```
sudo apt-get update
sudo apt-get upgrade
```

Use raspi-config to enable SSH and modify domain name (optional).

Raspbian comes with python3 preinstalled, but this is python 3.7 while we need python 3.8:

```
sudo apt-get install -y build-essential tk-dev libncurses5-dev libncursesw5-dev libreadline6-dev \
libdb5.3-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev \
libxml2-dev libxslt-dev python-dev gfortran libopenblas-dev liblapack-dev cython \
zlib1g-dev libjpeg-dev zlib1g-dev libffi-dev tar wget vim git nginx

wget https://www.python.org/ftp/python/3.8.6/Python-3.8.6.tgz
tar zxf Python-3.8.6.tgz
cd Python-3.8.6
./configure --enable-optimizations
make -j 4
sudo make altinstall
sudo rm -f /usr/bin/python3
sudo ln -s /usr/local/bin/python3.8 /usr/bin/python3
```

Make sure that Raspberry Pi is now using the proper python version:

```
python3 -V
Python 3.8.6
```

Update `/usr/bin/lsb_release` to be using distro version of python (modify `#!/usr/bin/python3 -Es` -> `#!/usr/bin/python3.7 -Es`)
then upgrade to the latest version of pip:

```
wget https://bootstrap.pypa.io/get-pip.py
sudo python3 get-pip.py
```

Make sure that Raspberry Pi is now using the proper version of pip:

```
pip3 -V
pip 20.2.3 from /usr/local/lib/python3.8/site-packages/pip (python 3.8)
```

Build required dependencies; building scipy will fail due to lack of free memory, so we need a bigger swap:

```
sudo /bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
sudo /sbin/mkswap /var/swap.1
sudo chmod 600 /var/swap.1
sudo /sbin/swapon /var/swap.1

sudo pip3 install Cython numpy scipy lxml
```

Install required python libraries:

```
pip3 install SimpleWebSocketServer xmltodict jsonpickle obspy
```

Once complete -- remove swap:

```
sudo /sbin/swapoff /var/swap.1
sudo rm /var/swap.1
```

I have encountered some problems using OBSPY with 32-bit Raspbian: https://github.com/obspy/obspy/issues/2683

There are two files that you need to patch: `obspy/io/mseed/core.py` and `obspy/io/mseed/headers.py` (they should be located in your python `dist-packages` folder).

#### core.py.patch

```
--- core.py	2020-10-26 22:52:59.835824487 +0300
+++ /usr/local/lib/python3.6/dist-packages/obspy/io/mseed/core.py	2020-10-26 23:08:34.971142530 +0300
@@ -791,7 +791,7 @@
                 trace_attr['encoding'] = None
         # automatically detect encoding if no encoding is given.
         if trace_attr['encoding'] is None:
-            if trace.data.dtype.type == np.int32:
+            if trace.data.dtype == np.int32:
                 trace_attr['encoding'] = 11
             elif trace.data.dtype.type == np.float32:
                 trace_attr['encoding'] = 4
```

#### headers.py.patch

```
--- headers.py	2020-10-30 01:02:05.401142688 +0300
+++ /usr/local/lib/python3.6/dist-packages/obspy/io/mseed/headers.py	2020-10-26 23:12:59.350955683 +0300
@@ -137,6 +137,7 @@
               np.dtype(native_str("|S1")).type: "a",
               np.dtype(np.int16).type: "i",
               np.dtype(np.int32).type: "i",
+              np.dtype(np.intc).type: "i",
               np.dtype(np.float32).type: "f",
               np.dtype(np.float64).type: "d"}
 # as defined in libmseed.h
```

## Install backend

Copy python files to the backend folder and create an empty registry file:

```
mkdir -p /opt/apatit/backend/data
cp globals.py monitor.py poller.py publisher.py server.py /opt/apatit/backend
echo '[]' > /opt/apatit/backend/data/devices.json
```

To run the backend server:

```
python3 server.py
```

## Install frontend

To build frontend refer to https://bitbucket.org/dsys-ru/frontend/src/master/
Copy the `dist` folder to your RaspberryPi:

```
scp -r dist pi@raspberry:/tmp
```

SSH to you raspberry then move the dist to proper location:

```
sudo mv /tmp/dist/* /opt/apatit/frontend/
sudo ln -s /opt/apatit/frontend /var/www/apatit
```

## Setup Nginx

Nginx is used as a reverse proxy. Just copy the file `nginx/default.conf` to `/etc/nginx/conf.d/` and restart:

```
systemctl restart nginx
```